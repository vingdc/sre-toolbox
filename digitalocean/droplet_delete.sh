#!/bin/bash
# This will delete ALL droplets in your account. proceed with caution!!!

doctl compute droplet list | awk '{ print $1 }' > servers.csv
sed '1d' servers.csv > servers_tmp.csv
input="servers_tmp.csv"
while IFS= read -r var
do
  doctl compute droplet delete -f "$var"
  echo "$var" droplet deleted
done < "$input"
echo done