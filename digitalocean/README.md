# Digital Ocean #

Create multiple droplets in digital ocean.

### Instructions ###

* Get your access tokens from digital ocean - https://www.digitalocean.com/docs/api/create-personal-access-token/
* Replace TOTAL, DIGITALOCEAN_ACCESS_TOKEN and BASEIMAGE


Install DOCTL with brew
```
brew install doctl
```

Authorize your machine with the command below
```
doctl auth init
```

The following script will create 65 servers, and test with goss validate Replace DIGITALOCEAN_ACCESS_TOKEN with your provisioned keys.  Replace the --image value with the base image ID you want to use. 

to list all the snapshots/backup in your DO account, use

```
doctl compute image list
```

### What the script does ###

This creates 65 droplets and generates the keys required to access them. The base image was prepared initially and saved as a snapshot in digitalocean. 


### Authors ###

* co-authored with https://github.com/brennebeck