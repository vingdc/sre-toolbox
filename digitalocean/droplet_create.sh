#!/bin/bash

DIGITALOCEAN_ACCESS_TOKEN="XXXXX"
TOTAL=65
BASEIMAGE=123456
counter=1
while [ $counter -le $TOTAL ]
do
echo "=== CREATING ${counter} BASE MACHINES ==="
# echo $counter
COUNTNAME="HM-team-${counter}"
echo "Generating key..."
ssh-keygen -f "${COUNTNAME}" -N ""

echo "Importing key..."
doctl compute ssh-key import "${COUNTNAME}" --public-key-file "${COUNTNAME}.pub"
sleep 1

DROPLETNAME="${COUNTNAME}-`/bin/date +%Y-%m`"
echo "Creating ${DROPLETNAME} now..."
DROPLETKEY_SSHKEY=$(doctl compute ssh-key list | grep "${COUNTNAME}" | awk '{ print $3 }')
sleep 1

doctl compute droplet create $DROPLETNAME --size s-1vcpu-2gb --image $BASEIMAGE --region sgp1 --ssh-keys "$DROPLETKEY_SSHKEY" --tag-names hackmanila-teams-final,${COUNTNAME}

DROPLETIP=$(doctl compute droplet list | grep "${COUNTNAME}"| awk '{ print $3}')
DROPLETID=$(doctl compute droplet list | grep "${COUNTNAME}"| awk '{ print $1}')
echo "COUNTNAME, DROPLETNAME, DROPLETIP, DROPLETID"
echo "${COUNTNAME}, ${DROPLETNAME}, ${DROPLETIP}, ${DROPLETID}"
echo "${COUNTNAME},${DROPLETNAME},${DROPLETIP},${DROPLETID}" >> log.csv

((counter++))
sleep 3
done
sleep 140

doctl compute droplet list --tag-name hackmanila-teams-final | awk '{ print $1"," $2"," $3 }' 2>&1 servers.csv
echo
SSHID=$(doctl compute droplet list --tag-name hackmanila-teams-final | awk 'NR>1 { print $1}' | tail -1)
echo $SSHID

echo "=== TESTING SERVER ==="
doctl compute ssh $SSHID --ssh-command "/usr/local/bin/goss -g /root/goss.yaml validate"

echo DONE
