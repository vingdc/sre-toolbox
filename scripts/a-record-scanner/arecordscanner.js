/**
 * @file Get A records of domains from NGINX config files
 * @author AgentImage
 * @version 1.0.2
 */

/* Load deps */
var Scanner = require("./lib/scanner.js");

/* Read domain list */
var dir = process.argv[2];
var output = process.argv[3];

/* Run scanner */
new Scanner(dir,output);

