# Scanner

Look up A records of domains extracted from nginx config files.

## Usage
* Run the following. 
```sh
node arecordscanner.js "/directory/of/nginx/configs" 
```

* When the script finishes, open `output.txt` created in the folder where `arecordscanner.js` is located.

