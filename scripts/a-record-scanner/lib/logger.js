var Fs = require("fs");

var logger = function() {
	
	var that = this;
	
	that.filename = __dirname + "/../output.txt";
	
}

logger.prototype.record = function(message) {
	
	var that = this;
	
	Fs.appendFileSync( that.filename, message + "\r\n" );
	console.log(message);
	
}

logger.prototype.reset = function() {
	
	var that = this;
	
	Fs.writeFileSync( that.filename, "" );
	
}

module.exports = logger;
