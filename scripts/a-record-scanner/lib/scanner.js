var fs = require( 'fs' );
var path = require( 'path' );
var process = require( "process" );
var dns = require('dns');
var Logger = require("./logger.js");

var scanner = function(dir) {
	
	var that = this;
	
	/* Create log file */
	that.logger = new Logger();
	that.logger.reset();
	
	/* Loop through all the files in the dir */
	fs.readdir( dir, function( err, files ) {
		if( err ) {
			console.error( "Could not list the directory.", err );
			process.exit( 1 );
		} 

		files.forEach( function( file, index ) {
			that.getLine(dir + "/" + file,1,file,that);		
		} );
	});
}

scanner.prototype.getLine = function(filename, line_no, orig_file,context) {
	
	var that = context;
	
	var stream = fs.createReadStream(filename, {
		flags: 'r',
		encoding: 'utf-8',
		fd: null,
		mode: 0666,
		bufferSize: 64 * 1024
	});

	var fileData = '';
	stream.on('data', function(data){
		fileData += data;

		// The next lines should be improved
		var lines = fileData.split("\n");

		if(lines.length >= +line_no){
			stream.destroy();

			var regex = /([a-z0-9|-]+\.)*[a-z0-9|-]+\.[a-z]+/ig;
			var words = lines[+line_no].split(" ");

			words.forEach( function(v,i) {
				if ( regex.test(v) ) {
					v = v.replace(";","");
					dns.lookup(v,function(err,address,family) {
						that.logger.record(orig_file + "," + v + ","+address);
					});
				}
			});


		}
	});

}

module.exports = scanner;