#!/bin/bash

crt=$(find $1 -type f -name '*.crt')
key=$(find $1 -type f -name '*.key')
csr=$(find $1 -type f -name '*.csr')

echo "KEY : "
openssl rsa -noout -modulus -in $key | openssl md5

echo "CSR : "
openssl req -noout -modulus -in $csr | openssl md5

echo "CRT : "
openssl x509 -noout -modulus -in $crt | openssl md5

